'use strict';

/**
 * @ngdoc overview
 * @name elisyam
 * @description
 * # elisyam
 *
 * Main module of the application.
 */
angular
  .module('elisyam', [
      'ui.router'
    ])
  // .factory('comunication', function() {
  //   return {
  //     message: 'vacio',
  //     getMessage: function() {
  //       return this.message;
  //     },
  //     setMessage: function(msg) {
  //       this.message = msg;
  //     }
  //   };
  // })
  .config(function ($stateProvider, $urlRouterProvider,$locationProvider) {
    $locationProvider.html5Mode(false);
    $urlRouterProvider.otherwise('/');
    $stateProvider
    .state('elisyam', {
      url: "/",
      abstract: true,
      template: '<ui-view/>'
    })
    .state('elisyam.inicio', {
      url:'Inicio',
      template: '<div inicio></div>'
    })
    .state('elisyam.opcion', {
      url:'opcion',
      template: '<div opcion></div>',
      data: {'elisyam':'Elisyam'}
    })
    .state('elisyam.opcion1', {
      url:'opcion1',
      template: '<div opcion1></div>',
      data: {'elisyam':'Elisyam'}
    })
    .state('elisyam.opcion2', {
      url:'opcion2',
      template: '<div opcion2></div>',
      data: {'elisyam':'Elisyam'}
    })
    .state('elisyam.opcion3', {
      url:'opcion3',
      template: '<div opcion3></div>',
      data: {'elisyam':'Elisyam'}
    })
    .state('elisyam.opcion4', {
      url:'opcion4',
      template: '<div opcion4></div>',
      data: {'elisyam':'Elisyam'}
    })
    .state('elisyam.opcion5', {
      url:'opcion5',
      template: '<div opcion5></div>',
      data: {'elisyam':'Elisyam'}
    })
    .state('elisyam.opcion6', {
      url:'opcion6',
      template: '<div opcion6></div>',
      data: {'elisyam':'Elisyam'}
    })
    .state('elisyam.opcion7', {
      url:'opcion7',
      template: '<div opcion7></div>',
      data: {'elisyam':'Elisyam'}
    })
    .state('elisyam.opcion8', {
      url:'opcion8',
      template: '<div opcion8></div>',
      data: {'elisyam':'Elisyam'}
    })
  });
