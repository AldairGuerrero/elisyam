(function () {
  'use strict';
  angular.module('elisyam').directive('opcion', opcion);
  opcion.$inject = ['$rootScope','$compile'];
  function  opcion($rootScope,$compile){
    return {
      restrict: 'EA',
      templateUrl: 'app/views/opciones/opcion.html',
      replace: true,
      link: function(scope, element) {
        $compile(element.contents())($rootScope);
      },
      controller:'opcionController'
    };
  }
  angular.module('elisyam').directive('opcion1', opcion1);
  opcion1.$inject = ['$rootScope','$compile'];
  function  opcion1($rootScope,$compile){
    return {
      restrict: 'EA',
      templateUrl: 'app/views/opciones/opcion1.html',
      replace: true,
      link: function(scope, element) {
        $compile(element.contents())($rootScope);
      },
      controller:'opcionController'
    };
  }
  angular.module('elisyam').directive('opcion2', opcion2);
  opcion2.$inject = ['$rootScope','$compile'];
  function  opcion2($rootScope,$compile){
    return {
      restrict: 'EA',
      templateUrl: 'app/views/opciones/opcion2.html',
      replace: true,
      link: function(scope, element) {
        $compile(element.contents())($rootScope);
      },
      controller:'opcionController'
    };
  }
  angular.module('elisyam').directive('opcion3', opcion3);
  opcion3.$inject = ['$rootScope','$compile'];
  function  opcion3($rootScope,$compile){
    return {
      restrict: 'EA',
      templateUrl: 'app/views/opciones/opcion3.html',
      replace: true,
      link: function(scope, element) {
        $compile(element.contents())($rootScope);
      },
      controller:'opcionController'
    };
  }
  angular.module('elisyam').directive('opcion4', opcion4);
  opcion4.$inject = ['$rootScope','$compile'];
  function  opcion4($rootScope,$compile){
    return {
      restrict: 'EA',
      templateUrl: 'app/views/opciones/opcion4.html',
      replace: true,
      link: function(scope, element) {
        $compile(element.contents())($rootScope);
      },
      controller:'opcionController'
    };
  }
  angular.module('elisyam').directive('opcion5', opcion5);
  opcion5.$inject = ['$rootScope','$compile'];
  function  opcion5($rootScope,$compile){
    return {
      restrict: 'EA',
      templateUrl: 'app/views/opciones/opcion5.html',
      replace: true,
      link: function(scope, element) {
        $compile(element.contents())($rootScope);
      },
      controller:'opcionController'
    };
  }
  angular.module('elisyam').directive('opcion6', opcion6);
  opcion6.$inject = ['$rootScope','$compile'];
  function  opcion6($rootScope,$compile){
    return {
      restrict: 'EA',
      templateUrl: 'app/views/opciones/opcion6.html',
      replace: true,
      link: function(scope, element) {
        $compile(element.contents())($rootScope);
      },
      controller:'opcionController'
    };
  }
  angular.module('elisyam').directive('opcion7', opcion7);
  opcion7.$inject = ['$rootScope','$compile'];
  function  opcion7($rootScope,$compile){
    return {
      restrict: 'EA',
      templateUrl: 'app/views/opciones/opcion7.html',
      replace: true,
      link: function(scope, element) {
        $compile(element.contents())($rootScope);
      },
      controller:'opcionController'
    };
  }
  angular.module('elisyam').directive('opcion8', opcion8);
  opcion8.$inject = ['$rootScope','$compile'];
  function  opcion8($rootScope,$compile){
    return {
      restrict: 'EA',
      templateUrl: 'app/views/opciones/opcion8.html',
      replace: true,
      link: function(scope, element) {
        $compile(element.contents())($rootScope);
      },
      controller:'opcionController'
    };
  }
})();
