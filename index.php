
<!DOCTYPE html>
<html lang="en" ng-app="elisyam">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Elisyam - Login</title>
        <meta name="description" content="Elisyam is a Web App and Admin Dashboard Template built with Bootstrap 4">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Google Fonts -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
        <script>
          WebFont.load({
            google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="app/assets/img/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="app/assets/img/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="app/assets/img/favicon-16x16.png">
        <!-- Stylesheet -->
        <link rel="stylesheet" href="app/assets/vendors/css/base/bootstrap.min.css">
        <link rel="stylesheet" href="app/assets/vendors/css/base/elisyam-1.5.min.css">
        <!-- sweet alert -->
        <link rel="stylesheet" href="app/plugins/sweetalert/sweetalert.css">
    </head>
    <body class="bg-white" ng-controller="loginController">
        <!-- Begin Preloader -->
        <div id="preloader">
            <div class="canvas">
                <img src="app/assets/img/logo.png" alt="logo" class="loader-logo">
                <div class="spinner"></div>
            </div>
        </div>
        <!-- End Preloader -->
        <!-- Begin Container -->
        <div class="container-fluid no-padding h-100">
            <div class="row flex-row h-100 bg-white">
                <!-- Begin Left Content -->
                <div class="col-xl-8 col-lg-6 col-md-5 no-padding">
                    <div class="elisyam-bg background-01">
                        <div class="elisyam-overlay overlay-01"></div>
                        <div class="authentication-col-content mx-auto">
                            <h1 class="gradient-text-01">
                                BIENVENIDO A Elisyam!
                            </h1>
                            <span class="description">
                                Although the results at a large pot drink in the warm-up at the bow of my mass poverty.
                            </span>
                        </div>
                    </div>
                </div>
                <!-- End Left Content -->
                <!-- Begin Right Content -->
                <div class="col-xl-4 col-lg-6 col-md-7 my-auto no-padding">
                    <!-- Begin Form -->
                    <div class="authentication-form mx-auto">
                        <div class="logo-centered">
                            <a href="db-default.html">
                                <img src="assets/img/logo.png" alt="logo">
                            </a>
                        </div>
                        <h3>Ingresar a Elisyam</h3>
                        <form>
                          <div class="group material-input">
          							    <input type="text" required>
    							           <span class="highlight"></span>
    							           <span class="bar"></span>
    							           <label>Email</label>
                            </div>
                            <div class="group material-input">
            							    <input type="password" required>
            							    <span class="highlight"></span>
            							    <span class="bar"></span>
            							    <label>Contraseña</label>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col text-left">
                                <div class="styled-checkbox">
                                    <input type="checkbox" name="checkbox" id="remeber">
                                    <label for="remeber">Recordarme</label>
                                </div>
                            </div>
                            <div class="col text-right">
                                <a href="pages-forgot-password.html">Recuperar Contraseña ?</a>
                            </div>
                        </div>
                        <div class="sign-btn text-center">
                            <a ng-click="loginapp()" class="btn btn-lg btn-gradient-01">Ingresar</a>
                        </div>
                        <div class="register">
                            Ya te registraste?
                            <br>
                            <a href="app/pages/pages-register.html">Crea una Cuenta</a>
                        </div>
                    </div>
                    <!-- End Form -->
                </div>
                <!-- End Right Content -->
            </div>
            <!-- End Row -->
        </div>
        <!-- End Container -->
        <!-- Begin Vendor Js -->
        <script src="app/assets/vendors/js/base/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.4/angular.min.js" charset="utf-8"></script>
        <script src="app/assets/vendors/js/base/core.min.js"></script>
        <!-- End Vendor Js -->
        <!-- Begin Page Vendor Js -->
        <script src="app/assets/vendors/js/app/app.min.js"></script>
        <!-- End Page Vendor Js -->
        <script src="app/scripts/controllers/login/loginController.js"></script>
        <script src="app/plugins/sweetalert/sweetalert.min.js"></script>
    </body>
</html>
